package Lock;

import java.util.concurrent.TimeUnit;

class HoldLockThread implements Runnable{
    private String lockA;
    private String lockB;

    public HoldLockThread(String lockA,String lockB){
        this.lockA=lockA;
        this.lockB=lockB;
    }
    @Override
    public void run() {
        synchronized (this.lockA){
            System.out.println(Thread.currentThread().getName()+"\t 自己持有："+lockA+"\t 尝试获得:"+lockB);
            try {TimeUnit.SECONDS.sleep(2);} catch (InterruptedException e) {e.printStackTrace(); }
            synchronized (lockB){
                System.out.println(Thread.currentThread().getName()+"\t 自己持有："+lockB+"\t 尝试获得:"+lockA);
            }
        }
    }
}
/**
 * @Author: chenoyongqiao
 * @Description: 死锁例子:死锁是指两个或以上线程在执行过程中因争抢资源造成的一种互相等待情况
 *               ThreadBBB	 自己持有：lockB	 尝试获得:lockA
 *               ThreadAAA	 自己持有：lockA	 尝试获得:lockB
 * @Date:Created in 9:47 2021/9/1
 * @Modified By:
 */
public class DealLockDemo {
    public static void main(String[] args) {
        String lockA = "lockA";
        String lockB = "lockB";
        new Thread(new HoldLockThread(lockA,lockB),"ThreadAAA").start();
        new Thread(new HoldLockThread(lockB,lockA),"ThreadBBB").start();
    }
}
