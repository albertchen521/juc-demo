package Lock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author: chenoyongqiao
 * @Description: 多线程直接按顺序调用，实现A -> B -> C 三个线程启动
 * A打印5次 B打印10次 C打印15次
 * <p>
 * Synchronized 和Lock区别
 * 1.原始构成：
 * Synchronized是关键字属于JVM层面
 * monitorenter(底层是通过monitor对象完成，其实wait/notify等方法也是依赖monitor对象
 * 只用同步块或方法中才能调用wait/notify等方法
 * Lock是具体的类（java.util.concurrent.locks.Lock)是api层的锁
 * <p>
 * 2.使用方法：
 * Synchronized 不需要用户手动释放锁，当synchronized代码执行完后系统会自动让线程释放对锁的占用
 * ReentrantLock则需要用户手动释放锁，若没有释放锁会导致死锁
 * <p>
 * 3.等待释放可中断
 * Synchronized不可以中断
 * ReentrantLock可以中断
 * <p>
 * 4.加锁是否公平
 * Synchronized默认非公平锁
 * ReentrantLock默认非公平锁，当ReentrantLock（true）为公平锁
 * <p>
 * 5.锁绑定多个条件condition
 * Synchronized没有
 * Reetrantlock用来实现分组唤醒需要唤醒的线程们，可以精准唤醒，而不是像Synchronized那样随机唤醒一个或是唤醒全部
 * @Date:Created in 17:58 2021/8/16
 * @Modified By:
 */
public class SyncAndReentrantLockDemo {

    public static void main(String[] args) {
        ShareResource shareResource = new ShareResource();
        for (int i = 0; i < 3; i++) {
            new Thread(() -> {
                try {
                    shareResource.print();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }, "A").start();
            new Thread(() -> {
                try {
                    shareResource.print();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }, "B").start();
            new Thread(() -> {
                try {
                    shareResource.print();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }, "C").start();
        }
    }
}

class ShareResource {
    Lock lock = new ReentrantLock();
    private int number = 1; // a:1 b:2 c:2
    private Condition c1 = lock.newCondition();
    private Condition c2 = lock.newCondition();
    private Condition c3 = lock.newCondition();

    public void print() throws InterruptedException {
        String threadName = Thread.currentThread().getName();
        switch (threadName) {
            case "A":
                printA(2);
                break;
            case "B":
                printB(2);
                break;
            case "C":
                printC(2);
                break;
            default:
                System.out.println("异常参数");
                break;
        }

    }

    public void printA(int i) throws InterruptedException {
        lock.lock();
        try {
            // 1 判断
            while (number != 1) {
                c1.await();
            }
            // 2.干活
            for (int j = 0; j < i; j++) {
                System.out.println(Thread.currentThread().getName() + "\t " + j);
            }
            // 3.通知
            number = 2;
            c2.signal();

        } finally {
            lock.unlock();
        }

    }

    public void printB(int i) throws InterruptedException {
        lock.lock();
        try {
            // 1 判断
            while (number != 2) {
                c2.await();
            }
            // 2.干活
            for (int j = 0; j < i; j++) {
                System.out.println(Thread.currentThread().getName() + "\t " + j);
            }
            // 3.通知
            number = 3;
            c3.signal();

        } finally {
            lock.unlock();
        }

    }

    public void printC(int i) throws InterruptedException {
        lock.lock();
        try {
            // 1 判断
            while (number != 3) {
                c3.await();
            }
            // 2.干活
            for (int j = 0; j < i; j++) {
                System.out.println(Thread.currentThread().getName() + "\t " + j);
            }
            // 3.通知
            number = 1;
            c1.signal();

        } finally {
            lock.unlock();
        }

    }


}
