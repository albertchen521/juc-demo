/**
 * @Author: chenoyongqiao
 * @Description: instance = new SingleModelDemo()在实例化的过程中是分步的
 *               1. 内存分配空间，这是一个物理的地址，里面现在还没有东西
 *               2. 将SingleModelDemo的属性方法等内容放到这个空间
 *               3. 将instance这个对象的指针指向这个内存空间    此时instance !=null
 *               指令重排可能会打乱这个顺序，例如132，这个打乱在单线程下是没有关系的，但是在多线程情况下，当发生这种情况
 *               if(instance ==null){} return instance,这是返回的instaance其实指向一个空的内存地址
 * @Date:Created in 14:03 2021/8/10
 * @Modified By:
 */
public class SingleModelDemo {
    private String name;
    // 增加volatile 禁止指令重排
    private static volatile SingleModelDemo instance = null;
    private  SingleModelDemo(){
        System.out.println("类初始化");
    }
    private static SingleModelDemo getInstance(){
        // 双端检索机制 DCL  double check lock   DCL的单例模式
        if(instance==null){
            synchronized (SingleModelDemo.class){
                // 防止实列初始化过程中，实列已经分配内存空间，但是这个地址里面还没有东西的情况
                // 第一次线程正在实列化new SingleModelDemo()还未结束，这时第二线程进入if(instance==null)通过，会再实例化一次
                if (instance==null){
                    instance = new SingleModelDemo();
                }
            }

        }
        return instance;
    }

    public static void main(String[] args) {
        /*System.out.println(SingleModelDemo.getInstance()==SingleModelDemo.getInstance());
        System.out.println(SingleModelDemo.getInstance()==SingleModelDemo.getInstance());
        System.out.println(SingleModelDemo.getInstance()==SingleModelDemo.getInstance());
        System.out.println(SingleModelDemo.getInstance()==SingleModelDemo.getInstance());*/
        for (int i=0;i<10;i++){
            new Thread(()->{
                //System.out.println(Thread.currentThread().getName()+SingleModelDemo.getInstance().name);
            },"线程"+i).start();
        }
    }
}


