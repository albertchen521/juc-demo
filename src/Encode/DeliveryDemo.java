package Encode;

/**
 * <pre>
 *      反码取模演示
 * </pre>
 *
 * @ClassName DeliveryDemo
 * @Author chenyq185
 * @Date 2022/4/1 9:16
 * @Version 1.0
 */
public class DeliveryDemo {
    public static void main(String[] args) {
        // 8使用原码表示为 0000 1000
        System.out.println(8>>2); //0000 10
        System.out.println(8<<2); //0010 0000

        /*
        * 反码、补码的存在是为了解决机器计算中产生的减法
        * 真值 2 + (-2) =0
        * 原码表示 0010 +  1010 = 1010 = -2 显然不符合预期
        * 反码表示 0010 +  1101 = 1111 = （原码表示）1000 = -0  从真值意义上看是符合预期的但是不够完美
        * 补码表示 0010 +  1110 = 10000 =(丢掉最高位) 0000 = 0
        * */
    }
}
