package ThreathPool;

import java.util.concurrent.*;

/**
 * @Author: chenoyongqiao
 * @Description: 线程的创建
 * 1. 继承Thread类
 * 2. 实现runable接口
 * 3. 实现callable接口
 * @Date:Created in 14:29 2021/8/24
 * @Modified By:
 */
public class ThreadPoolDemo {
    public static void main(String[] args) {
        // System.out.println(Runtime.getRuntime().availableProcessors());
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                2,
                5,
                1L,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(3),
                Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());
        //验证自定义缓存池,默认拒绝策略  抛出异常
        //testThreadPool(threadPoolExecutor, null);
        //验证拒绝策略 任务返回调用线程 即main 让main休眠1s加以验证
        testThreadPool(threadPoolExecutor,new ThreadPoolExecutor.CallerRunsPolicy());
        //验证拒绝策略 直接丢弃任务
        //testThreadPool(threadPoolExecutor,new ThreadPoolExecutor.DiscardPolicy());
        //验证拒绝策略 丢弃队列中等待最久任务
        //testThreadPool(threadPoolExecutor, new ThreadPoolExecutor.DiscardOldestPolicy());
        // 让main休眠1s加以验证CallerRunsPolicy
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void testThreadPool(ExecutorService threadPool, RejectedExecutionHandler rejectedPolicy) {
        ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) threadPool;
        if (rejectedPolicy != null) {
            threadPoolExecutor.setRejectedExecutionHandler(rejectedPolicy);
        }
        // 模拟n个用户
        try {
            for (int i = 0; i < 10; i++) {
                threadPoolExecutor.submit(() -> {
                    System.out.println(Thread.currentThread().getName() + "执行任务");
                    try {
                        Thread.sleep(1000L);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            threadPool.shutdown();
        }
    }
}
