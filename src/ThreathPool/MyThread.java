package ThreathPool;

import sun.awt.SunHints;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @Author: chenoyongqiao
 * @Description:
 * @Date:Created in 11:53 2021/9/30
 * @Modified By:
 */
public class MyThread {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        new Thread(()->{
            System.out.println(Thread.currentThread().getName());
        },"sss").start();
        new Thread(
                new MyThreadFromExtendsThread()   // 指定线程名字
        ).start();

        new Thread(
                new MyThreadFromImplementsRunnable(),"MyThreadFromImplementsRunnable"
        ).start();
       // callable使用适配器模式完成传参
        FutureTask<String> futureTask = new FutureTask<>(new MyThreadFromImplementsCallable());
        new Thread(futureTask,"MyThreadFromImplementsCallable").start();
        /*Thread tCallThread = new Thread(futureTask,"AA");
        tCallThread.start();*/
        while (!futureTask.isDone()){
           // 类似自旋CAS
        }
        System.out.println("result:"+ futureTask.get());
    }
}

class MyThreadFromExtendsThread extends Thread{
    @Override
    public void run() { System.out.println(Thread.currentThread().getName()+"--this extends Thread Class");
    }
}
class MyThreadFromImplementsRunnable implements Runnable{
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"--this implements Runnable interface");
    }

}

class MyThreadFromImplementsCallable implements Callable<String>{
    @Override
    public String call() throws Exception {
        System.out.println(Thread.currentThread().getName()+"--this implements callable interface");
        return "execute success";
    }
}
