package ThreathPool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author: chenoyongqiao
 * @Description:
 * @Date:Created in 13:53 2021/10/12
 * @Modified By:
 */
public class ThreeMethodCreateThreadPool {
    public static void main(String[] args) {
        ExecutorService threadPoolFixed = Executors.newFixedThreadPool(5);      //LinkedBlockingQueue
        ExecutorService threadPoolSingle = Executors.newSingleThreadExecutor();           //LinkedBlockingQueue
        ExecutorService threadPoolCached = Executors.newCachedThreadPool();               //SynchronousQueue
        // 验证固定线程数线程池
        //testThreadPool(threadPoolFixed);
        // 验证单线程数线程池
        //testThreadPool(threadPoolSingle);
        // 验证带缓存线程池
        testThreadPool(threadPoolCached);
    }

    private static void testThreadPool(ExecutorService threadPoolExecutor) {
        // 模拟n个用户
        try {
            for (int i = 0; i < 10; i++) {
                threadPoolExecutor.submit(() -> {
                    System.out.println(Thread.currentThread().getName() + "执行任务");
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            threadPoolExecutor.shutdown();
        }
    }
}
