package ThreathPool;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @Author: chenoyongqiao
 * @Description: 使用线程池来执行多线程任务
 * @Date:Created in 20:12 2021/10/11
 * @Modified By:
 */
public class ComparmToThreadPool {
    public static void main(String[] args) throws InterruptedException {
        Long strat = System.currentTimeMillis();
        final Random random = new Random();
        final List<Integer> list = new ArrayList<>();
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        for (int i = 0; i <100000 ; i++) {
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    list.add(random.nextInt());
                }
            });
        }
        executorService.shutdown();
        //executorService.shutdownNow();
        executorService.awaitTermination(1, TimeUnit.DAYS);   // 阻塞当前线程
        //executorService.awaitTermination(1, TimeUnit.MILLISECONDS);   // 阻塞当前线程
        System.out.println("时间："+(System.currentTimeMillis()-strat));
        System.out.println(Thread.currentThread().getName()+"--大小："+list.size());

    }
}
