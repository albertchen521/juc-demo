package ThreathPool;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @Author: chenoyongqiao
 * @Description:
 * 时间：44604
 * 大小：100000
 * @Date:Created in 20:12 2021/10/11
 * @Modified By:
 */
public class ComparmToTraditon {
    public static void main(String[] args) throws InterruptedException {
        Long strat = System.currentTimeMillis();
        final Random random = new Random();
        final List<Integer> list = new ArrayList<>();
        for (int i = 0; i <100000 ; i++) {
            Thread thread = new Thread("tardition"){
                @Override
                public void run(){
                  list.add(random.nextInt());
                }
            };
            thread.start();
            thread.join();  //使得main线程等待thread线程执行完毕，避免并发
        }
        System.out.println("时间："+(System.currentTimeMillis()-strat));
        System.out.println("大小："+list.size());

    }
}
