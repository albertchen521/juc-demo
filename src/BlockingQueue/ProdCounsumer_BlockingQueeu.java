package BlockingQueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author: chenoyongqiao
 * @Description: volatile/CAS/aromicIntefer/BlockingQueue/线程交互/原子引用
 * @Date:Created in 14:36 2021/8/19
 * @Modified By:
 */
public class ProdCounsumer_BlockingQueeu {
    public static void main(String[] args) {
        ResourceData resourceData = new ResourceData(new ArrayBlockingQueue(10));
        new Thread(()->{
            System.out.println("生产线程启动");
            try {
                resourceData.myProc();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"prod").start();
        new Thread(()->{
            System.out.println("消费线程启动");
            try {
                resourceData.myConsumer();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"consumer").start();

        //主线程休眠5s
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //停止生产消费
        System.out.println("5s结束生产消费");
        resourceData.stop();
    }
}

class ResourceData {
    private volatile boolean runFalg = true;// 默认开启进行生产消费
    private volatile AtomicInteger atomicInteger = new AtomicInteger();

    BlockingQueue<String> blockingQueue = null;

    public ResourceData(BlockingQueue blockingQueue) {
        this.blockingQueue = blockingQueue;
        System.out.println(blockingQueue.getClass().getName());
    }

    public void myProc() throws Exception {
        String data = null;
        boolean reValue;
        while (runFalg) {
            data = atomicInteger.incrementAndGet() + "";
            reValue = blockingQueue.offer(data, 2L, TimeUnit.SECONDS);
            if (reValue) {
                System.out.println(Thread.currentThread().getName() + "\t 插入队列" + data + "成功");
            } else {
                System.out.println(Thread.currentThread().getName() + "\t 插入队列" + data + "失败");
            }
            TimeUnit.SECONDS.sleep(1);
        }
        System.out.println("停止生产，runFlag=" + runFalg);
    }

    public void myConsumer() throws Exception {
        String result = null;
        while (runFalg) {
            result = blockingQueue.poll(2L, TimeUnit.SECONDS);
            if (result == null || result.equalsIgnoreCase("")) {
                runFalg = false;
                System.out.println("超过两秒未取到数据，消费退出");
                return;
            }
            System.out.println(Thread.currentThread().getName() + "\t 消费队列" + result + "成功");
        }
        System.out.println("停止消费，runFlag=" + runFalg);
    }
    public void stop(){
        runFalg = false;
    }
}
