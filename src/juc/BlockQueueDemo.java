package juc;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @Author: chenoyongqiao
 * @Description: 阻塞队列:当队列中是空时，从队列中获取元素的操作会被阻塞
 *                        当队列中时满时，向队列中添加元素的操作会被阻塞
 * @Date:Created in 11:26 2021/8/16
 * @Modified By:
 */
public class BlockQueueDemo {
    public static void main(String[] args) throws InterruptedException {
        //List lsit = new ArrayList();
        BlockingQueue<String> blockQueue = new ArrayBlockingQueue<>(3);
        System.out.println(blockQueue.offer("a",2L, TimeUnit.SECONDS));
        System.out.println(blockQueue.offer("a",2L, TimeUnit.SECONDS));
        System.out.println(blockQueue.offer("a",2L, TimeUnit.SECONDS));
        System.out.println(blockQueue.offer("a",2L, TimeUnit.SECONDS));


        /*blockQueue.put("a");
        blockQueue.put("b");
        blockQueue.put("c");
        blockQueue.put("d");*/

       /* System.out.println(blockQueue.offer("a"));
        System.out.println(blockQueue.offer("b"));
        System.out.println(blockQueue.offer("v"));
        System.out.println(blockQueue.offer("g"));
        System.out.println(blockQueue.element());*/



        /*System.out.println(blockQueue.add("a"));
        System.out.println(blockQueue.add("b"));
        System.out.println(blockQueue.add("c"));
        //System.out.println(blockQueue.add("d"));
        System.out.println(blockQueue.element());

        System.out.println(blockQueue.remove());
        System.out.println(blockQueue.remove());
        System.out.println(blockQueue.remove());*/


    }
}
