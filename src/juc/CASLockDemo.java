package juc;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @Author: chenoyongqiao
 * @Description: 自旋锁，尝试获取锁的线程不会阻塞，而是采用不断循环的方式尝试获取锁
 * @Date:Created in 10:11 2021/8/12
 * @Modified By:
 */
public class CASLockDemo {
    private AtomicReference<Thread> lockThread = new AtomicReference<>();
    private void myLock(){
        System.out.println(Thread.currentThread().getName()+"/t come in myLock");
        // 获取当前进入该方法的线程
        Thread thread = Thread.currentThread();
        while (!lockThread.compareAndSet(null,thread)){
            //如果lockThread为空，则预期值与实际值相同，判断为True,取反之后while（false)进入循环，除非上一个进入该方法的线程
            //将lockThread修改为null  由于Atomic的value被volatile修饰过，具有可见性，这时当前线程跳出循环
        }
    }
    private void myUnlock(){
        Thread thread = Thread.currentThread();
        lockThread.compareAndSet(thread,null);
        System.out.println(Thread.currentThread().getName()+"/t involked myUnlock");
    }
    public static void main(String[] args) {
        CASLockDemo casLockDemo = new CASLockDemo();

        new Thread(()->{
            casLockDemo.myLock();
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            casLockDemo.myUnlock();
        },"AA").start();
        new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            casLockDemo.myLock();
            casLockDemo.myUnlock();
        },"BB").start();
    }
}
