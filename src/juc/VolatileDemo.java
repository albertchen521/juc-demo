package juc;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author: chenoyongqiao
 * @Description:
 * @Date:Created in 8:10 2021/8/8
 * @Modified By:
 */
public class VolatileDemo {

    public static void main(String[] args) {
        MyDate myDate = new MyDate();
        new Thread(() -> {
            while (myDate.runFlag) {
                //当线程2修改了主内存中的runFlag,线程1也不会识到，循环不会停止
            }
            System.out.println(Thread.currentThread().getName() + "任务结束");
        }, "线程1").start();
        new Thread(() -> {
            try {
                System.out.println(Thread.currentThread().getName() + "进入");
                //线程2休眠1秒保证线程1先拿到runFlag
                TimeUnit.SECONDS.sleep(1);
                // 线程操控资源类
                myDate.runFlag = false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "线程2").start();
        //System.out.println("主线程结束");
        for (int i = 0; i < 10000; i++) {
            new Thread(()->{
                myDate.addAndAdd();
            },"验证原子性"+i).start();
        }
        try {
            //主线程休眠4s，保证验证原子性的100个线程跑完
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("验证原子性多次增加后值,volatile修饰："+myDate.number);
        System.out.println("验证原子性多次增加后值，原子类处理"+myDate.atomicInteger);
    }
}

class MyDate {
    //增加volatile修改后runFlag具有可见性
    volatile Boolean runFlag = true;
    //Boolean  runFlag = true;
    volatile Integer number = 0;
    // 使用原子类保证原子性
    AtomicInteger atomicInteger = new AtomicInteger();
    void addTo10() {
        this.number = 10;
    }
    void addAndAdd(){
        this.number++;
        atomicInteger.getAndAdd(1);
    }
}
