package juc;

import java.util.concurrent.CountDownLatch;

/**
 * @Author: chenoyongqiao
 * @Description: 计数器
 * @Date:Created in 10:21 2021/8/16
 * @Modified By:
 */
public class CountDownLatchDemo {
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(6);
        for (int i = 1; i <=6 ; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"\t 国被灭亡");
                countDownLatch.countDown();
            }, CountryEnums.forEach_CountryEnum(i).getRetMessage()).start();
        }
        countDownLatch.await();
        System.out.println(Thread.currentThread().getName()+"\t ___________秦国大一统");
    }
}
