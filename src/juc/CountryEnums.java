package juc;

/**
 * @Author: chenoyongqiao
 * @Description: 国家枚举
 * @Date:Created in 10:41 2021/8/16
 * @Modified By:
 */
public enum  CountryEnums {
    ONE(1,"齐"),TWO(2,"楚"),THREE(3,"赵"),FOUR(4,"燕"),FIVE(5,"魏"),SIX(6,"韩");
    private Integer retCode;
    private String retMessage;
    CountryEnums(Integer retCode,String retMessage){
        this.retCode=retCode;
        this.retMessage=retMessage;
    }
    public static CountryEnums forEach_CountryEnum(int index){
        CountryEnums[] myArray = CountryEnums.values();
        for (CountryEnums element :myArray){
            if (index==element.getRetCode()){
                return element;
            }
        }
        return null;
    }

    public Integer getRetCode() {
        return retCode;
    }

    public void setRetCode(Integer retCode) {
        this.retCode = retCode;
    }

    public String getRetMessage() {
        return retMessage;
    }

    public void setRetMessage(String retMessage) {
        this.retMessage = retMessage;
    }
}
