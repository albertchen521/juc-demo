package juc;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @Author: chenoyongqiao
 * @Description: 循环屏障 从0 增加在一定值
 *              让一组线程达到一个屏障
 * @Date:Created in 11:07 2021/8/16
 * @Modified By:
 */
public class CyclicBarrierDemo {
    public static void main(String[] args) {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(7,()->{
            System.out.println("****召唤神龙");
        });
        for (int i = 1; i <=7 ; i++) {
            new Thread(()->{
                try {
                    cyclicBarrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+"\t 收集的龙珠");
            },String.valueOf(i)).start();
        }

    }
}
