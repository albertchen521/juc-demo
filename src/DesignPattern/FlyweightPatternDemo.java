package DesignPattern;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: chenoyongqiao
 * @Description: 享元模式
 * 意图：运用共享技术有效地支持大量细粒度的对象。
 *
 * 主要解决：在有大量对象时，有可能会造成内存溢出，我们把其中共同的部分抽象出来，如果有相同的业务请求，直接返回在内存中已有的对象，避免重新创建。
 *
 * 何时使用： 1、系统中有大量对象。 2、这些对象消耗大量内存。 3、这些对象的状态大部分可以外部化。 4、这些对象可以按照内蕴状态分为很多组，当把外蕴对象从对象中剔除出来时，每一组对象都可以用一个对象来代替。 5、系统不依赖于这些对象身份，这些对象是不可分辨的。
 *
 * 如何解决：用唯一标识码判断，如果在内存中有，则返回这个唯一标识码所标识的对象。
 *
 * 关键代码：用 HashMap 存储这些对象。
 *
 * 应用实例： 1、JAVA 中的 String，如果有则返回，如果没有则创建一个字符串保存在字符串缓存池里面。 2、数据库的数据池。
 * @Date:Created in 14:11 2021/10/15
 * @Modified By:
 */
public class FlyweightPatternDemo {
    public static void main(String[] args) {
        TreeNode treeNode = new TreeNode(1,1,TreeFactory.getTree("杨树"));
        treeNode.use();
        TreeNode treeNode1 = new TreeNode(1,2,TreeFactory.getTree("杨树"));
        treeNode1.use();
        TreeNode treeNode2 = new TreeNode(1,3,TreeFactory.getTree("槐树"));
        treeNode2.use();
        TreeNode treeNode4 = new TreeNode(1,3,TreeFactory.getTree("槐树"));
        treeNode4.use();

        System.out.println("池中树的总数："+TreeFactory.countTreeNum());
    }

}
class TreeNode{
    int x;
    int y;
    Tree tree;
    public void use(){
        System.out.println("该树位于：("+x+","+y+"),品种为："+tree.getName());
    }

    public TreeNode(int x, int y, Tree tree) {
        this.x = x;
        this.y = y;
        this.tree = tree;
    }
}
class TreeFactory{
    public  static Map<String,Tree> treeMap = new HashMap<>();
    public static Tree getTree( String treeName){
        if (treeMap.containsKey("treeName")){
            return treeMap.get("treeName");
        }
        Tree tree = new Tree(treeName,"1");
        treeMap.put(treeName,tree);
        //System.out.println(tree.toString());
        return tree;

    }
    public static int countTreeNum(){
        return treeMap.size();
    }
}
class Tree{
    private String name;
    private String hight;

    public Tree(String name, String hight) {
        this.name = name;
        this.hight = hight;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.hashCode()+"--name :"+name;
    }
}
